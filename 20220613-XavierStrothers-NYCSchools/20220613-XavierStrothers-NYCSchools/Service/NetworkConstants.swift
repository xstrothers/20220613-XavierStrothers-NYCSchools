//
//  NetworkConstants.swift
//  20220613-XavierStrothers-NYCSchools
//
//  Created by Xavier Strothers on 6/13/22.
//

import Foundation

//API's

struct NetworkConstants {
    static let schoolList = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    static let satInfo = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
}
