//
//  URLSession.swift
//  20220613-XavierStrothers-NYCSchools
//
//  Created by Xavier Strothers on 6/13/22.
//
import Foundation

protocol Session {
    func getData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: Session {
    
    func getData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }.resume()
        
    }
    
}
