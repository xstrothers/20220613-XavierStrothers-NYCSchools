//
//  Model.swift
//  20220613-XavierStrothers-NYCSchools
//
//  Created by Xavier Strothers on 6/13/22.
//

import Foundation

struct SchoolInformation: Decodable {
    let dbn: String
    let schoolName: String
    var satInfo: SchoolSATInformation?

    enum CodingKeys: String, CodingKey {
        case dbn, satInfo
        case schoolName = "school_name"
    }
}

