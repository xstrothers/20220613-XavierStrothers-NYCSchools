//
//  TableViewCell.swift
//  20220613-XavierStrothers-NYCSchools
//
//  Created by Xavier Strothers on 6/13/22.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    let schoolLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "School: "
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createCell() {
        // constraining
        self.contentView.addSubview(schoolLabel)
        schoolLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16).isActive = true
        schoolLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -16).isActive = true
        schoolLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16).isActive = true
        schoolLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16).isActive = true
       
    }
    //configure cell w/ data from network call
    func configure(schoolName: String?) {
        self.schoolLabel.text = "School: \(schoolName ?? "???")"
    }
}
